<?php
$factory->define(App\Cases::class,function (Faker\Generator $faker) {

    return [
        'nrakti' => 'nrAkti' . rand(1,1000),
        'caseNumber' => 'caseNumber' . rand(1,1000),
        'plaintiff' => 'plaintiff' . rand(1,10),
        'defendent' => 'defendent' . rand(1,10),
        'thirdParties' => 'thirdParties' . rand(1,10),
        'object' => 'object' . rand(1,10),
        'court' => rand(1,4),
        'month' => rand(1,12),
        'userId' => rand(1,20),
        'claimValue' => rand(1000,18000),
        'judge' => $faker->name .' '. $faker->lastName,
        'archived' => $faker->randomElement(['Po','Jo']),
        'lawyerId' => rand(1,20)
    ];


});
