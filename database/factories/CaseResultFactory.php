<?php
$factory->define(App\CaseResult::class,function (Faker\Generator $faker) {

    return [
        'caseId' => rand(1,20),
        'courtResult' => rand(1,7),
        'courtDecision' => 'courtDecision' . rand(1,50),
        'userId' => rand(1,20),
        'decisionNumber' => rand(1000,100000),
        'resultDate' => \Carbon\Carbon::now(),
    ];


});
