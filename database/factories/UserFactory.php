<?php
$factory->define(App\User::class,function (Faker\Generator $faker) {

    return [
      'nameSurname' => $faker->name . ' ' . $faker->lastName,
      'email' => $faker->email,
      'password' => \Illuminate\Support\Facades\Hash::make('password'),
      'department' => $faker->randomElement(['1','2','3','4']),
      'userType' => rand(1,4),
      'status' => $faker->randomElement(['1','2']),
    ];


});
