<?php
$factory->define(App\CaseSession::class,function (Faker\Generator $faker) {
    return [
        'caseNumber' => 'caseNumber' . rand(1,1000),
        'description' => 'description' . rand(12112,12312123)
    ];
});
