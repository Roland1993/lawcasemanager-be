<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lcm_Case', function (Blueprint $table) {
            $table->increments('id');
//            $table->string('nrakti', 100);
//            $table->string('caseNumber', 50);
//            $table->string('plaintiff', 1000);
//            $table->string('defendent', 1000);
//            $table->string('thirdParties', 1000);
//            $table->string('object', 1000);
//            $table->string('court', 1000);
//            $table->integer('month');
//            $table->integer('userId');
//            $table->integer('claimValue');
//            $table->string('judge', 1000);
//            $table->string('archived', 1000);
//            $table->integer('lawyerId');
            $table->integer('idCreator')->nullable();
            $table->integer('idEditor')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lcm_Case');
    }
}
