<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaseResultTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lcm_CaseResult', function (Blueprint $table) {
//            $table->increments('id');
            $table->integer('caseId');
            $table->tinyInteger('courtResult');
            $table->string('courtDecision', 1000)->nullable;
            $table->integer('userId')->nullable;
            $table->string('decisionNumber', 50)->nullable;
            $table->date('resultDate')->nullable;
            $table->integer('idCreator')->nullable;
            $table->integer('idEditor')->nullable;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lcm_CaseResult');
    }
}
