<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lcm_User', function (Blueprint $table) {
            $table->increments('id');
//            $table->string('nameSurname',200);
//            $table->string('email',200);
            $table->string('password',500);
//            $table->string('department',200);
//            $table->tinyInteger('userType');
//            $table->string('status', 12);
            $table->integer('idCreator')->nullable();
            $table->integer('idEditor')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lcm_User');
    }
}
