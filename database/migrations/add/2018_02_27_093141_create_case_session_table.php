<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaseSessionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lcm_CaseSession', function (Blueprint $table) {
//            $table->increments('id');
//            $table->string('caseNumber', 50);
//            $table->dateTime('caseDate')->nullable;
//            $table->tinyInteger('caseHour')->nullable;
//            $table->tinyInteger('caseMinute')->nullable;
//            $table->text('description')->nullable;
//            $table->text('descriptionSecond')->nullable;
            $table->integer('idEditor')->nullable;
            $table->integer('idCreator')->nullable;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lcm_CaseSession');
    }
}
