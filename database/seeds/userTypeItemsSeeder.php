<?php

use Illuminate\Database\Seeder;

class userTypeItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lcm_user_type_data_pool')->insert([
            'userTypeText' => 'Përdorues Bazik',
        ]);

        DB::table('lcm_user_type_data_pool')->insert([
            'userTypeText' => 'Përdorues Normal',
        ]);

        DB::table('lcm_user_type_data_pool')->insert([
            'userTypeText' => 'Super Përdorues',
        ]);

        DB::table('lcm_user_type_data_pool')->insert([
            'userTypeText' => 'Admin',
        ]);
    }
}
