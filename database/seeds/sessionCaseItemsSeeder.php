<?php

use Illuminate\Database\Seeder;

class sessionCaseItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\CaseSession::class,15)->create();
    }
}
