<?php

use Illuminate\Database\Seeder;

class statusItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lcm_status_data_pool')->insert([
            'statusText' => 'I Punësuar',
        ]);

        DB::table('lcm_status_data_pool')->insert([
            'statusText' => 'Jo i punësuar',
        ]);
    }
}
