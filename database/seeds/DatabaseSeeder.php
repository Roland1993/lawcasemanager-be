<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UserTableSeeder::class);
         $this->call(CaseTableSeeder::class);
         $this->call(CaseResultTableSeeder::class);
         $this->call(courtItemsSeeder::class);
         $this->call(monthItemsSeeder::class);
         $this->call(sessionCaseItemsSeeder::class);
         $this->call(userTypeItemsSeeder::class);
         $this->call(caseResultItemsSeeder::class);
         $this->call(departmentItemsSeeder::class);
         $this->call(statusItemsSeeder::class);
    }
}
