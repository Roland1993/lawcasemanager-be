<?php

use Illuminate\Database\Seeder;

class caseResultItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lcm_case_result_data_pool')->insert([
            'caseResult' => 'Fituar',
        ]);

        DB::table('lcm_case_result_data_pool')->insert([
            'caseResult' => 'Humbur',
        ]);

        DB::table('lcm_case_result_data_pool')->insert([
            'caseResult' => 'Fituar pjesërisht',
        ]);

        DB::table('lcm_case_result_data_pool')->insert([
            'caseResult' => 'Pushuar',
        ]);

        DB::table('lcm_case_result_data_pool')->insert([
            'caseResult' => 'Transferuar',
        ]);

        DB::table('lcm_case_result_data_pool')->insert([
            'caseResult' => 'Jo e përfaqësuar',
        ]);

        DB::table('lcm_case_result_data_pool')->insert([
            'caseResult' => 'aktive',
        ]);
    }
}
