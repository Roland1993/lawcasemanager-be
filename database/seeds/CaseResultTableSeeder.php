<?php

use Illuminate\Database\Seeder;

class CaseResultTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\CaseResult::class,15)->create();
    }
}
