<?php

use Illuminate\Database\Seeder;

class monthItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lcm_month_data_pool')->insert([
            'monthText' => 'Janar',
        ]);

        DB::table('lcm_month_data_pool')->insert([
            'monthText' => 'Shkurt',
        ]);

        DB::table('lcm_month_data_pool')->insert([
            'monthText' => 'Mars',
        ]);

        DB::table('lcm_month_data_pool')->insert([
            'monthText' => 'Prill',
        ]);

        DB::table('lcm_month_data_pool')->insert([
            'monthText' => 'Maj',
        ]);

        DB::table('lcm_month_data_pool')->insert([
            'monthText' => 'Qershor',
        ]);

        DB::table('lcm_month_data_pool')->insert([
            'monthText' => 'Korrik',
        ]);

        DB::table('lcm_month_data_pool')->insert([
            'monthText' => 'Gusht',
        ]);

        DB::table('lcm_month_data_pool')->insert([
            'monthText' => 'Shtator',
        ]);

        DB::table('lcm_month_data_pool')->insert([
            'monthText' => 'Tetor',
        ]);

        DB::table('lcm_month_data_pool')->insert([
            'monthText' => 'Nëntor',
        ]);

        DB::table('lcm_month_data_pool')->insert([
            'monthText' => 'Dhjetor',
        ]);
    }
}
