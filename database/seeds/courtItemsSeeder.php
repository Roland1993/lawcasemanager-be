<?php

use Illuminate\Database\Seeder;

class courtItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lcm_court_data_pool')->insert([
            'courtText' => 'Shkalla e parë',
        ]);

        DB::table('lcm_court_data_pool')->insert([
            'courtText' => 'Shkalla e dytë',
        ]);

        DB::table('lcm_court_data_pool')->insert([
            'courtText' => 'Shkalla e tretë',
        ]);

        DB::table('lcm_court_data_pool')->insert([
            'courtText' => 'Shkalla e katërt',
        ]);

    }
}
