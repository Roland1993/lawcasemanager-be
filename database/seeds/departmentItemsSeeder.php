<?php

use Illuminate\Database\Seeder;

class departmentItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lcm_department_data_pool')->insert([
            'departmentText' => 'Zyra e përfaqësimit në gjykatat kombëtare',
        ]);

        DB::table('lcm_department_data_pool')->insert([
            'departmentText' => 'Zyra e këshillimit dhe koordinimit ndërministror',
        ]);

        DB::table('lcm_department_data_pool')->insert([
            'departmentText' => 'Zyra e përfaqësimit në gjykatat e huaja dhe arbitrazhi',
        ]);

        DB::table('lcm_department_data_pool')->insert([
            'departmentText' => 'Zyra e inspektimit',
        ]);
    }
}
