<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MonthDataPool extends Model
{
    protected $table = 'lcm_month_data_pool';
}
