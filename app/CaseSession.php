<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaseSession extends Model
{
    protected $table = 'lcm_CaseSession';

    protected $fillable = [
      'caseNumber',
      'caseDate',
      'caseHour',
      'caseMinute',
      'description',
      'descriptionSecond',
      'idEditor',
      'idCreator'
    ];
}
