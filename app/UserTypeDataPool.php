<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTypeDataPool extends Model
{
    protected $table = 'lcm_user_type_data_pool';
}
