<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourtDataPool extends Model
{
    protected $table = 'lcm_court_data_pool';
}
