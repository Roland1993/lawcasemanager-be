<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cases extends Model
{

    protected $table = 'lcm_Case';

    protected $fillable = [
        'nrakti',
        'plaintiff',
        'defendent',
        'thirdParties',
        'object',
        'court',
        'month',
        'userId',
        'claimValue',
        'judge',
        'archived',
        'lawyerId',
        'idCreator',
        'idEditor'
    ];

    public function result() {
        return $this->hasOne('\App\CaseResult', 'caseId');
    }

}
