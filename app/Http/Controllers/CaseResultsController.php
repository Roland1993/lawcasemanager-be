<?php

namespace App\Http\Controllers;


use App\CaseResult;
use App\CaseResultDataPool;
use Illuminate\Http\Request;

class CaseResultsController extends Controller
{
    public function getCaseResults(Request $request)
    {
        $caseResults = CaseResultDataPool::select('id', 'caseResult')->get();

        return response()->json(["caseResults" => $caseResults], 200);

    }

    /**
     * @TODO update this route to include id
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateCaseResult(Request $request)
    {
        $userID = $request->user()->id;

        try {

            $caseResult = CaseResult::select('id')->where('caseId', $request->id)->first();

            $input = $request->all();
            $caseResult->fill($input);
            $caseResult->idEditor = $userID;

            $caseResult->save();

            return response()->json([
                "message" => "Rezultati i cështjes u ndryshua me sukses"
            ], 200);

        } catch (\Exception $e) {
            return response()->json([
                "message" => $e->getMessage()
            ], 500);
        }
    }

    public function deleteCaseResult(Request $request)
    {

        try {
            $caseResult = CaseResult::where('caseId', '=', $request->id)->delete();
            return response()->json([
                "message" => "Cështja u fshi me sukses"
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                "message" => $e->getMessage()
            ], 500);
        }
    }
}