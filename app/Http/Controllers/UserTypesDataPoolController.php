<?php

namespace App\Http\Controllers;

use App\UserTypeDataPool;
use Illuminate\Http\Request;

class UserTypesDataPoolController extends Controller
{
    public function getUserTypes (Request $request)
    {
        $userTypes = UserTypeDataPool::select('id','userTypeText')->get();

        return response()->json(["userTypes" => $userTypes], 200);
    }
}
