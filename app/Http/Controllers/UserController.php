<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\JWTAuth;
use Auth;
use App\User;
class UserController extends Controller
{
    /**
     * @var \Tymon\JWTAuth\JWTAuth
     */
    protected $jwt;

    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }

    public function login(Request $request)
    {

        try {
            if (!$token = $this->jwt->attempt($request->only('email', 'password'))) {
                return response()->json(['message' => 'Kredencialet tuaj janë të gabuara'], 404);
            }
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], 498);
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], 500);
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent' => $e->getMessage()], 500);
        }


        $userID = Auth::user()->id;

        if (Auth::user()->status == 2) {
            return response()->json([
                "message" => "Nuk jeni të autorizuar"
            ], 404);
        }

        $response = compact('token');

        $user = DB::table('lcm_user AS u')
            ->join('lcm_user_type_data_pool AS udp', 'u.userType', '=', 'udp.id')
            ->join('lcm_department_data_pool AS ddp', 'u.department', '=', 'ddp.id')
            ->join('lcm_status_data_pool AS sdp', 'u.status', '=', 'sdp.id')
            ->select(DB::raw('u.nameSurname, u.email, u.status, sdp.statusText, u.userType, u.department, udp.userTypeText, ddp.departmentText'))
            ->where('u.id', '=', $userID)
            ->first();

        $response['user'] = $user;
        return response()->json($response);

    }

    public function checkEmail(Request $request)
    {
        $exists = false;

        if (User::where('email', '=', $request->email)->exists()) {
            $exists = true;
        }

        return response()->json($exists);
    }

    public function getLawyers(Request $request)
    {
        $department = $request->user()->department;
        $userType = $request->user()->userType;
        try {
            if ($userType === 3) {
                $lawyers = User::select('id', 'nameSurname')
                    ->where('userType', 1)
                    ->where('department', $department)
                    ->get();
            } else {
                $lawyers = User::select('id', 'nameSurname')->where('userType', 1)->get();
            }
            return response()->json(["lawyers" => $lawyers], 200);
        } catch (\Exception $e) {
            return response()->json(["message" => $e->getMessage()], 500);
        }
    }

    public function addNewUser(Request $request)
    {

        $userID = $request->user()->id;
        $userType = $request->user()->userType;

        if ($userType !== 4) {
            return response()->json([
                "message" => "Nuk keni të drejta aksesi për këtë veprim"
            ], 403);
        }

        $user = new User;

        $user->nameSurname = $request->nameSurname;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->department = $request->department;
        $user->userType = $request->userType;
        $user->status = $request->status;
        $user->idCreator = $userID;

        try {
            $user->save();
            return response()->json(["message" => "Përdoruesi u shtua me sukses"], 200);
        } catch (\Exception $e) {
            return response()->json([
                "message" => $e->getMessage()
            ], 500);
        }

    }

    public function changePassword(Request $request)
    {

        $userLoggedID = $request->user()->id;
        $userLoggedType = $request->user()->userType;

        if ($request->get('userID')) {
            $userIDtoEdit = $request->get('userID');
        } else {
            $userIDtoEdit = $userLoggedID;
        }


        $newPassword = Hash::make($request->newPassword);
        $user = User::findOrFail($userIDtoEdit);
        $userPassword = User::select('password')->where('id', $userIDtoEdit)->first()->makeVisible('password');

        if (Hash::check($request->oldPassword, $userPassword->password)) {
            try {
                $user->password = $newPassword;
                $user->idEditor = $userLoggedID;
                $user->save();

                return response()->json(["message" => "Fjalëkalimi u ndyshua me sukses"], 200);
            } catch (\Exception $e) {
                return response()->json(["message" => $e->getMessage()], 500);
            }
        }


        return response()->json(["message" => "Fjalëkalimi i vjetër nuk është i saktë! Fjalëkalimi nuk u ndryshua"], 403);


        $user = User::find($request->id);
        $newPasswordHashed = Hash::make($request->newPassword);

        if ($request->has('oldPassword')) {
            $oldPassword = Hash::make($request->oldPassword);
            $userPassword = User::select('password')->where('id', $request->id)->first()->makeVisible('password');

            if (Hash::check($request->oldPassword, $userPassword->password)) {
                $user->password = $newPasswordHashed;

                try {
                    $user->save();
                    return response()->json(["message" => "Password Changed"], 200);
                } catch (\Exception $e) {
                    return response()->json($e->getMessage(), 500);
                }
            } else {
                return response()->json(["Your old password is incorrect"], 404);
            }
        } else {
            $user->password = $newPasswordHashed;

            try {
                $user->save();
                return response()->json(["message" => "Password Changed"], 200);
            } catch (\Exception $e) {
                return response()->json($e->getMessage(), 500);
            }

        }

    }

    public function getUsers(Request $request)
    {
        $userType = $request->user()->userType;

        if ($userType !== 4) {
            return response()->json([
                "message" => "Nuk keni të drejta aksesi për këtë veprim"
            ], 403);
        }

        $userID = $request->user()->id;
        $department = $request->user()->department;
        $page = $request->get('page');
        $rowsPerPage = $request->get('rowsPerPage');
        $skip = ($page - 1) * $rowsPerPage;
        $order = null;
        $search = $request->get('search');
        $descending = $request->get('descending');
        $sortBy = $request->get('sortBy');

        $usersQuery = DB::table('lcm_user AS u')
            ->join('lcm_department_data_pool AS ddp', 'u.department', 'ddp.id')
            ->join('lcm_user_type_data_pool AS udp', 'u.userType', 'udp.id')
            ->join('lcm_status_data_pool AS sdp', 'u.status', 'sdp.id')
            ->select(DB::raw('u.id,u.nameSurname, u.email, u.userType, u.department, u.status, ddp.departmentText, udp.userTypeText, sdp.statusText'))
            ->where('u.id', '!=', $userID);

        if ($descending !== null) {
            if ($descending === true) {
                $order = 'desc';
            } else {
                $order = 'asc';
            }

            $usersQuery->orderBy($sortBy, $order);
        } else {
            $usersQuery->latest();
        }

        if ($search) {
            $usersQuery->where(function ($query) use ($search) {
                $query->orWhere('nameSurname', 'LIKE', '%' . $search . '%')
                    ->orWhere('email', 'LIKE', '%' . $search . '%')
                    ->orWhere('departmentText', 'LIKE', '%' . $search . '%')
                    ->orWhere('userTypeText', 'LIKE', '%' . $search . '%')
                    ->orWhere('statusText', 'LIKE', '%' . $search . '%');
            });
        }

        $countUsers = $usersQuery->count();
        $users = $usersQuery->skip($skip)->take($rowsPerPage)->get();

        try {
            return response()->json([
                "totalPages" => $countUsers,
                "users" => $users,
            ], 200);
        } catch (\Exception $e) {
            return response()->json(["message" => $e->getMessage()]);
        }

    }

    public function deleteUser(Request $request)
    {
        $userType = $request->user()->userType;

        if ($userType !== 4) {
            return response()->json([
                "message" => "Nuk keni të drejta aksesi për këtë veprim"
            ], 403);
        }

        $user = User::findOrFail($request->id);

        try {
            $user->delete();
            return response()->json(["message" => "Përdoruesi u fshi me sukses"], 200);
        } catch (\Exception $e) {
            return response()->json(["message" => $e->getMessage()], 500);
        }
    }

    /**
     * @TODO add userId in the request
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUser(Request $request)
    {
        $userType = $request->user()->userType;

        if ($userType !== 4) {
            return response()->json([
                "message" => "Nuk keni të drejta aksesi për këtë veprim"
            ]);
        }

        try {
            $user = User::findOrFail($request->id);

            $input = $request->all();

            $user->fill($input)->save();

            return response()->json(["message" => "Përdoruesi u ndryshua me sukses"], 200);
        } catch (\Exception $e) {
            return response()->json(["message" => $e->getMessage()], 500);
        }
    }

}