<?php

namespace App\Http\Controllers;

use App\CourtDataPool;
use Illuminate\Http\Request;

class CourtsDataPoolController extends Controller
{
    public function getCourts (Request $request)
    {
        $courts = CourtDataPool::select('id', 'courtText')->get();

        return response()->json(["courts" => $courts], 200);
    }
}
