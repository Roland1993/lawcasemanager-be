<?php

namespace App\Http\Controllers;

use App\DepartmentDataPool;
use Illuminate\Http\Request;

class DepartmentsDataPoolController extends Controller
{

    public function getDepartments (Request $request)
    {
        $departments = DepartmentDataPool::select('id', 'departmentText')->get();

        return response()->json(["departments" => $departments], 200);
    }
}
