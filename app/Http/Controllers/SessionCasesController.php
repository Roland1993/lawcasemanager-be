<?php

namespace App\Http\Controllers;

use App\CaseSession;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SessionCasesController extends Controller
{
    public function getSessionCases(Request $request)
    {
        $userType = $request->user()->userType;
        $userID = $request->user()->id;
        $userDepartment = $request->user()->department;
        $page = $request->get('page');
        $rowsPerPage = $request->get('rowsPerPage');
        $skip = ($page - 1) * $rowsPerPage;
        $order = null;
        $search = $request->get('search');
        $descending = $request->get('descending');
        $sortBy = $request->get('sortBy');

        $sessionCasesQuery = DB::table('lcm_casesession AS cs')
            ->leftJoin(DB::raw('(select distinct c.caseNumber, group_concat(u.department) as department, group_concat(u.id) as userID from lcm_case c join lcm_user u on c.lawyerId = u. id where c.caseNumber != "" group by c.caseNumber) as dis'), 'dis.caseNumber', '=', 'cs.caseNumber')
            ->select(DB::raw('cs.*, department, userID'));

        if ($descending !== null) {
            if ($descending === true) {
                $order = 'desc';
            } else {
                $order = 'asc';
            }
            $sessionCasesQuery->orderBy($sortBy, $order);
        }


        if ($search) {
            $sessionCasesQuery->where(function ($query) use ($search) {
                $query->orWhere('cs.caseNumber', 'LIKE', '%' . $search . '%')
                    ->orWhere('description', 'LIKE', '%' . $search . '%');
            });
        }


        if ($userType === 1) {
            $countSessionCases = $sessionCasesQuery->where('userID', '=', $userID)->count();
            $sessionCases = $sessionCasesQuery->where('userID', '=', $userID)->skip($skip)->take($rowsPerPage)->get();
        } else if ($userType === 3) {
            $countSessionCases = $sessionCasesQuery->where('department', '=', $userDepartment)->count();
            $sessionCases = $sessionCasesQuery->where('department', '=', $userDepartment)->skip($skip)->take($rowsPerPage)->get();
        } else {
            $countSessionCases = $sessionCasesQuery->count();
            $sessionCases = $sessionCasesQuery->skip($skip)->take($rowsPerPage)->get();
        }

        return response()->json([
            "sessionCases" => $sessionCases,
            "totalPages" => $countSessionCases
        ], 200);

    }

    /**
     * @TODO Refactor this method
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateSessionCase(Request $request)
    {
        $userType = $request->user()->userType;
        $userID = $request->user()->id;

        if ($userType === 2) {
            return response()->json([
                "message" => "Nuk keni të drejta aksesi për këtë veprim"
            ], 403);
        }

        try {
            $sessionCase = CaseSession::findOrFail($request->id);

            $sessionCase->caseNumber = $request->caseNumber;
            $sessionCase->description = $request->description;
            $sessionCase->idEditor = $userID;

            $sessionCase->save();

            return response()->json([
                "message" => "Seanca u ndryshua me sukses"
            ], 200);
        } catch (\Exception $e) {
            return response()->json(["message" => $e->getMessage()], 500);
        }
    }

    public function deleteSessionCase(Request $request)
    {

        try {
            $sessionCase = CaseSession::where('id', '=', $request->id)->delete();
            return response()->json([
                "message" => "Seanca u fshi me sukses"
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "message" => $e->getMessage()
            ], 500);
        }
    }

    /**
     * @TODO add case id in this route
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addSessionCase(Request $request)
    {
        $sessionCase = new CaseSession;

        try {
            $sessionCase->caseNumber = $request->caseNumber;
            $sessionCase->description = $request->description;

            $sessionCase->save();

            return response()->json([
                "message" => "Seanca u shtua me sukses"
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                "message" => $e->getMessage()
            ], 500);
        }
    }
}
