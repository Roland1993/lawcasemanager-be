<?php

namespace App\Http\Controllers;

use App\CaseResult;
use App\Cases;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CaseController extends Controller
{

    public function getCases(Request $request)
    {
        $userID = $request->user()->id;
        $userType = $request->user()->userType;
        $userDepartment = $request->user()->department;
        $page = $request->get('page');
        $rowsPerPage = $request->get('rowsPerPage');
        $skip = ($page - 1) * $rowsPerPage;
        $order = null;
        $columns = $request->get('columns');
        $search = $request->get('search');
        $descending = $request->get('descending');
        $sortBy = $request->get('sortBy');
        $filterTable = $request->get('filterTable');

        $casesQuery = DB::table('lcm_case AS c')
            ->join('lcm_caseresult AS cr', 'c.id', '=', 'cr.caseId')
            ->join('lcm_case_result_data_pool AS dp', 'cr.courtResult', '=', 'dp.id')
            ->join('lcm_user AS u', 'u.id', '=', 'c.lawyerId')
            ->join('lcm_department_data_pool AS ddp', 'u.department', '=', 'ddp.id')
            ->join('lcm_month_data_pool AS month', 'c.month', '=', 'month.id')
            ->join('lcm_court_data_pool AS court', 'c.court', '=', 'court.id')
            ->select(DB::raw('c.*, dp.id AS caseResultID, cr.courtDecision, cr.decisionNumber, dp.caseResult, ddp.departmentText, u.nameSurname, u.department, month.monthText, court.courtText'));

        if ($descending !== null) {
            if ($descending) {
                $order = 'desc';
            } else {
                $order = 'asc';
            }
            $casesQuery->orderBy($sortBy, $order);
        }

        if ($search) {
            $casesQuery->where(function ($query) use ($columns, $search) {
                foreach ($columns as $column) {
                    if (!($column["value"] === "veprime")) {
                        $query->orWhere($column["value"], 'LIKE', '%' . $search . '%');
                    }
                }
            });

        }


        if ($filterTable) {
            $archived = $request->filterTable["archived"];
            $lawyer = $request->filterTable["lawyer"];
            $caseResult = $request->filterTable["caseResult"];
            $month = $request->filterTable["month"];
            $year = $request->filterTable["year"];
            $department = $request->filterTable["department"];

            if ($archived === "0") {
                $archived = null;
            }
            if ($archived) {
                $casesQuery->where('archived', $archived);
            }

            if ($caseResult) {
                $casesQuery->where('caseResult', $caseResult);
            }


            if ($month) {
                $casesQuery->where('monthText', '=', $month);
            }

            if ($year) {
                $casesQuery->whereYear('c.created_at', '=', $year);
            }

            if ($lawyer && $userType !== 1) {
                $casesQuery->where('nameSurname', $lawyer);
            }

            if ($department && ($userType === 4 || $userType === 2)) {
                $casesQuery->where('ddp.departmentText', '=', $department);
            }

        }

        if ($userType === 1) {
            $countCases = $casesQuery->where('lawyerId', $userID)->count();
            $cases = $casesQuery->where('lawyerId', $userID)->skip($skip)->take($rowsPerPage)->get();
        } else if ($userType === 3) {
            $countCases = $casesQuery->where('department', $userDepartment)->count();
            $cases = $casesQuery->where('department', $userDepartment)->skip($skip)->take($rowsPerPage)->get();
        } else {
            $countCases = $casesQuery->count();
            $cases = $casesQuery->skip($skip)->take($rowsPerPage)->get();
        }

        return response()->json([
            "totalPages" => $countCases,
            "cases" => $cases
        ], 200);
    }

    public function addNewCase(Request $request)
    {

        $userID = $request->user()->id;
        $userType = $request->user()->userType;

        if ($userType === 1 || $userType === 2) {
            return response()->json([
                "message" => "Nuk keni të drejta aksesi për këtë veprim"
            ], 403);
        }

        $year = date("Y");
        $month = date("n");
        $day = date("d");
        $hours = date("H");
        $minutes = date("i");
        $seconds = date("s");
        $nrCase = $year . $month . $day . $hours . $minutes . $seconds;

        $case = new Cases;

        $case->caseNumber = $nrCase;
        $case->nrakti = $request->nrakti;
        $case->plaintiff = $request->plaintiff;
        $case->defendent = $request->defendent;
        $case->thirdParties = $request->thirdParties;
        $case->object = $request->object;
        $case->court = $request->court;
        $case->month = $request->month;
        $case->userId = $userID;
        $case->claimValue = $request->claimValue;
        $case->judge = $request->judge;
        $case->archived = $request->archived;
        $case->lawyerId = $request->lawyerId;
        $case->idCreator = $userID;

        try {
            $case->save();

            $caseResult = new CaseResult();
            $caseResult->caseId = $case->id;
            $caseResult->courtResult = 7;
            $caseResult->userId = $userID;
            $caseResult->idCreator = $userID;

            $caseResult->save();

            return response()->json(["message" => "Cështja u shtua me sukses"], 200);
        } catch (\Exception $e) {
            return response()->json(["message" => $e->getMessage()], 500);
        }
    }

    /**
     * @TODO add case id in the request
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateCase(Request $request, $id)
    {
        $userID = $request->user()->id;
        $userType = $request->user()->userType;

        if ($userType === 1 || $userType === 2) {
            return response()->json([
                "message" => "Nuk keni të drejta aksesi për këtë veprim"
            ], 403);
        }

        try {
            $case = Cases::findOrFail($request->id);

            $input = $request->all();

            $case->fill($input);
            $case->idEditor = $userID;

            $case->save();

            return response()->json(["Cështja u ndryshua me sukses"], 200);
        } catch (\Exception $e) {
            return response()->json(["message" => $e->getMessage()], 500);
        }
    }

    /**
     * @TODO add case id in the route
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteCase(Request $request)
    {
        try {
            $case = Cases::where('id', '=', $request->id)->delete();
            return response()->json(["Cështja u fshi me sukses"], 200);
        } catch (\Exception $e) {
            return response()->json(["message" => $e->getMessage()], 500);
        }
    }

    public function checkCaseNumber(Request $request)
    {
        $userType = $request->user()->userType;
        $userID = $request->user()->id;
        $userDepartment = $request->user()->department;
        $caseNumber = $request->get('caseNumber');
        $caseNumberExist = false;


        if ($userType === 1) {
            if (Cases::where('lawyerId', '=', $userID)->where('caseNumber', '=', $caseNumber)->exists()) {
                $caseNumberExist = true;
            }
        } else if ($userType === 3) {
            if (
            DB::table('lcm_case AS c')
                ->join('lcm_user AS u', 'c.lawyerId', '=', 'u.id')
                ->where('u.department', '=', $userDepartment)
                ->where('c.caseNumber', '=', $caseNumber)
                ->exists()
            ) {
                $caseNumberExist = true;
            }
        } else {
            if (Cases::where('caseNumber', '=', $caseNumber)->exists()) {
                $caseNumberExist = true;
            }

        }
        return response()->json($caseNumberExist);
    }

    public function getCaseNumbers(Request $request)
    {
        $userType = $request->user()->userType;
        $userID = $request->user()->id;
        $userDepartment = $request->user()->department;
        $caseNumber = $request->get('caseNumber');

        if ($userType === 1) {
            $caseNumbersQuery = Cases::select('caseNumber')
                ->where('lawyerId', '=', $userID)
                ->where('caseNumber', 'LIKE', $caseNumber . '%');
        } else if ($userType === 3) {
            $caseNumbersQuery = DB::table('lcm_case AS c')
                ->join('lcm_user AS u', 'c.lawyerId', '=', 'u.id')
                ->select(DB::raw('c.caseNumber, u.department'))
                ->where('u.department', '=', $userDepartment)
                ->where('caseNumber', 'LIKE', $caseNumber . '%');
        } else {
            $caseNumbersQuery = Cases::select('caseNumber')->where('caseNumber', 'LIKE', $caseNumber . '%');
        }

        $countCaseNumbers = $caseNumbersQuery->count();

        if ($countCaseNumbers > 50) {
            $caseNumbers = $caseNumbersQuery->take(50)->get();
        } else {
            $caseNumbers = $caseNumbersQuery->get();
        }

        return response()->json([
            "caseNumbers" => $caseNumbers
        ], 200);
    }
}
