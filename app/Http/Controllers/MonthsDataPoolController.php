<?php

namespace App\Http\Controllers;

use App\MonthDataPool;
use Illuminate\Http\Request;

class MonthsDataPoolController extends Controller
{
    public function getMonths (Request $request)
    {
        $months = MonthDataPool::select('id', 'monthText')->get();

        return response()->json(["months" => $months], 200);
    }
}
