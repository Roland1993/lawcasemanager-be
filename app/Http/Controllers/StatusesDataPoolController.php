<?php

namespace App\Http\Controllers;

use App\StatusDataPool;
use Illuminate\Http\Request;

class StatusesDataPoolController extends Controller
{
    public function getStatuses (Request $request)
    {
      $statuses = StatusDataPool::select('id', 'statusText')->get();

        return response()->json(["statuses" => $statuses], 200);
    }
}
