<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaseResult extends Model
{
    protected $table = 'lcm_caseresult';

    protected $fillable = [
      'caseId',
      'courtResult',
      'courtDecision',
      'userId',
      'decisionNumber',
      'resultDate',
      'idCreator',
      'idEditor'
    ];

    public function cases() {
        return $this->BelongsTo('\App\Cases', 'caseId');
    }

    public function dataPool() {
        return $this->belongsTo('\App\CaseResultDataPool', 'courtResult');
    }
}
