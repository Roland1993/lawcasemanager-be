<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepartmentDataPool extends Model
{
    protected $table = 'lcm_department_data_pool';
}
