<?php
/**
 * Created by PhpStorm.
 * User: GERMAN
 * Date: 28-Feb-18
 * Time: 11:54 AM
 */

if ( ! function_exists('config_path'))
{
    /**
     * Get the configuration path.
     *
     * @param  string $path
     * @return string
     */
    function config_path($path = '')
    {
        return app()->basePath() . '/config' . ($path ? '/' . $path : $path);
    }
}