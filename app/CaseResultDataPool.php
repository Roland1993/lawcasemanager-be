<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaseResultDataPool extends Model
{
    protected $table = 'lcm_case_result_data_pool';

    protected $fillable = ['caseResult'];

    public function result() {
        return $this->hasOne('\App\CaseResult', 'courtResult');
    }
}
