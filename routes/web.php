<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/auth/login', 'UserController@login');

$router->group([
    'middleware' => 'auth:api'
], function ($app) {

    /* CASE ROUTES */

    $app->post('/getCases', [
        'uses' => 'CaseController@getCases',
        'as' => 'casePage'
    ]);

    $app->post('/check/case/number', [
        'uses' => 'CaseController@checkCaseNumber',
        'as' => 'checkCaseNumber'
    ]);

    $app->post('/add/new/case', [
        'uses' => 'CaseController@addNewCase',
        'as' => 'addNewCase'
    ]);

    $app->put('/update/case', [
        'uses' => 'CaseController@updateCase',
        'as' => 'updateCase'
    ]);

    $app->delete('/delete/case', [
        'uses' => 'CaseController@deleteCase',
        'as' => 'deleteCase'
    ]);

    $app->post('/caseNumbers', [
        'uses' => 'CaseController@getCaseNumbers',
        'as' => 'getCaseNumbers'
    ]);


    /*CASE ROUTES*/

    /* ============================= */

    /*SESSION CASE ROUTES*/

    $app->post('/add/new/session/case', [
        'uses' => 'SessionCasesController@addSessionCase',
        'as' => 'addSessionCase'
    ]);

    $app->post('/get/session/case', [
        'uses' => 'SessionCasesController@getSessionCases',
        'as' => 'getSessionCases'
    ]);

    $app->put('/update/session/case', [
        'uses' => 'SessionCasesController@updateSessionCase',
        'as' => 'updateSessionCase'
    ]);

    $app->delete('/delete/session/case', [
        'uses' => 'SessionCasesController@deleteSessionCase',
        'as' => 'deleteSessionCase'
    ]);

    /*SESSION CASE ROUTES*/

    /* ============================= */

    /* USER ROUTES */

    $app->post('/getUsers', [
        'uses' => 'UserController@getUsers',
        'as' => 'UserPage'
    ]);

    $app->get('/getLawyers', [
        'uses' => 'UserController@getLawyers',
        'as' => 'getLayers'
    ]);

    $app->put('/change/password', [
        'uses' => 'UserController@changePassword',
        'as' => 'changePassword'
    ]);

    $app->post('/checkEmail', [
        'uses' => 'UserController@checkEmail',
        'as' => 'checkEmail'
    ]);

    $app->put('/update/user', [
        'uses' => 'UserController@updateUser',
        'as' => 'updateUser'
    ]);

    $app->delete('/deleteUser', [
        'uses' => 'UserController@deleteUser',
        'as' => 'deleteUser'
    ]);

    $app->post('/add/new/user', [
        'uses' => 'UserController@addNewUser',
        'as' => 'addNewUser'
    ]);


    /* USER ROUTES */

    /* ============================= */

    /* CASERESULT ROUTES */

    $app->get('/caseResults', [
        'uses' => 'CaseResultsController@getCaseResults',
        'as' => 'caseResults'
    ]);

    $app->put('/update/caseResult', [
        'uses' => 'CaseResultsController@updateCaseResult',
        'as' => 'updateCaseResult'
    ]);

    $app->delete('/delete/caseResult', [
        'uses' => 'CaseResultsController@deleteCaseResult',
        'as' => 'deleteCaseResult'
    ]);


    /* CASERESULT ROUTES */

    /* ============================= */


});

$router->group([
    'middleware' => 'auth:api',
    'prefix' => 'V2.0'
], function ($app) {

    /****************
     * CASES */
    $app->get('/cases', [
        'uses' => 'CaseController@getCases',
        'as' => 'cases.list'
    ]);

    $app->get('/cases/number_check', [
        'uses' => 'CaseController@checkCaseNumber',
        'as' => 'cases.number_check'
    ]);

    $app->post('/cases', [
        'uses' => 'CaseController@addNewCase',
        'as' => 'cases.store'
    ]);

    $app->put('/cases/{id}', [
        'uses' => 'CaseController@updateCase',
        'as' => 'cases.update'
    ]);

    $app->delete('/cases/{id}', [
        'uses' => 'CaseController@deleteCase',
        'as' => 'cases.delete'
    ]);

    // @TODO review this route
    $app->get('/cases/numbers', [
        'uses' => 'CaseController@getCaseNumbers',
        'as' => 'cases.get_numbers'
    ]);

    /*CASE ROUTES*/

    /* ============================= */

    /*SESSION CASE ROUTES*/

    $app->post('/cases/{caseId}/sessions', [
        'uses' => 'SessionCasesController@addSessionCase',
        'as' => 'cases.sessions.store'
    ]);

    $app->get('/cases/sessions', [
        'uses' => 'SessionCasesController@getSessionCases',
        'as' => 'cases.sessions.list'
    ]);

    $app->put('/cases/{caseId}/sessions/{sessionId}', [
        'uses' => 'SessionCasesController@updateSessionCase',
        'as' => 'cases.sessions.update'
    ]);

    $app->delete('/cases/{caseId}/sessions/{sessionId}', [
        'uses' => 'SessionCasesController@deleteSessionCase',
        'as' => 'cases.sessions.delete'
    ]);

    /*SESSION CASE ROUTES*/

    /* ============================= */

    /* USER ROUTES */

    $app->get('/users', [
        'uses' => 'UserController@getUsers',
        'as' => 'users.list'
    ]);

    $app->get('/lawyers', [
        'uses' => 'UserController@getLawyers',
        'as' => 'users.list_lawyers'
    ]);

    $app->put('/password/change', [
        'uses' => 'UserController@changePassword',
        'as' => 'password.change'
    ]);

    $app->get('/email/check', [
        'uses' => 'UserController@checkEmail',
        'as' => 'email.check'
    ]);

    $app->put('/users/{id}', [
        'uses' => 'UserController@updateUser',
        'as' => 'users.update'
    ]);

    $app->delete('/users/{id}', [
        'uses' => 'UserController@deleteUser',
        'as' => 'users.delete'
    ]);

    $app->post('/users', [
        'uses' => 'UserController@addNewUser',
        'as' => 'users.create'
    ]);


    /* USER ROUTES */

    /* ============================= */

    /* CASERESULT ROUTES */

    // @TODO review this route
    $app->get('/cases/results', [
        'uses' => 'CaseResultsController@getCaseResults',
        'as' => 'cases.results.list'
    ]);

    // @TODO review this route
    $app->put('cases/{id}/results', [
        'uses' => 'CaseResultsController@updateCaseResult',
        'as' => 'cases.results.update'
    ]);

    // @TODO review this route
    $app->delete('/cases/{id}/results', [
        'uses' => 'CaseResultsController@deleteCaseResult',
        'as' => 'cases.results.delete'
    ]);

    /* CASERESULT ROUTES */

    /* ============================= */
});