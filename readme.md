# Law-Case-Manager API with Lumen

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

# Installing
*First of all clone the project then follow the steps*
### step1
`composer install`

### step2
* copy the code from .env.example
* create new file and name it .env
* paste the code you have copied from .env.example into the new file (.env)

### step3
* Edit the .env file according to your database configuration (DB_DATABASE,DB_USERNAME,DB_PASSWORD)

### step4
`php artisan key:generate`

### step5
`php artisan jwt:secret`

### step6
`php artisan migrate`

### step7
`php artisan db:seed`

### step8
`php -S localhost:8000 -t public` *to start the application*

Documentation for the Lumen framework can be found on the [Lumen website](http://lumen.laravel.com/docs).


## License

The Lumen framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
